<?php
/**
 *  Chat - LogAll.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/logall.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/logs.class.php");

//データベース接続
DataBase::connect($conf);

//インスタンス作成
$logall_instance = new Logs($config);

//ログセット
$logall_instance->LogAll();

//ログ表示
echo $logall_instance->GetStatus();

//データベース切断
DataBase::DisConnect();

?>
