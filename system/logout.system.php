<?php
/**
 *  Chat - Logout.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/logout.config.php");
require_once("../config/systemlog.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/sessions.class.php");

//セッションstart
session_start();

//データベース接続
DataBase::connect($conf);

$login_id = $_SESSION["login_id"];
$token = $_SESSION["login_token"];

//インスタンス作成
$logout_instance = new Sessions($config);

if($logout_instance->LogOut( $login_id, $token)) {

    //ログアウト処理
    $logout_instance->DelNowLogIn();

    //セッション初期化
    session_unset();

}

//ログインstatus
echo $logout_instance->GetStatus();

//データベース切断
DataBase::DisConnect();
