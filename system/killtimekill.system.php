<?php
/**
 *  Chat - KillTimeaKill.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/killtimekill.config.php");
require_once("../config/systemlog.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/killtime.class.php");

//データベース接続
DataBase::connect($conf);

//インスタンス作成
$killtimekilla_instance = new KillTime($config);

//タイムアウト処理
$killtimekilla_instance->KillTimeKill();

//ステータス出力
echo $killtimekilla_instance->GetStatus();

DataBase::DisConnect();

?>
