<?php
/**
 *  Chat - NowLogin.System
 *  Author : Goma.Nanoha
 */

ini_set('display_errors', 1);

//Hader JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/nowlogin.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/sessions.class.php");

//データベース接続
DataBase::connect($conf);

//インスタンス作成
$nowlogin_instance = new Sessions($config);

//ログイン確認
$nowlogin_instance->NowLogIn();

//ステータス出力
echo $nowlogin_instance->GetStatus();

DataBase::DisConnect();
?>
