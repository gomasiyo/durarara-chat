<?php
/**
 *  Chat - LogUpdate.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/logupdate.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/logs.class.php");

//データベース接続
Database::connect($conf);

//インスタンス作成
$logupdata_instance = new Logs($config);

//ログセット
$logupdata_instance->LogUpdate($_POST["max_id"]);

//ログセット
echo $logupdata_instance->GetStatus();

//データベース切断
DataBase::DisConnect();

?>
