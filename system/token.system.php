<?php
/**
 *  Chat - Token.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../class/token.class.php");

session_start();

//インスタンス作成
$get_token = new Token();

if(empty($_SESSION["token"])){

    //セッションにトークンを追加
    $_SESSION["token"] = $get_token->GetToken();

}

//トークンを整形
echo $get_token->FairingToken($_SESSION["token"]);

