<?php
/**
 *  Chat - KillTimeUpdate.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/killtimeupdate.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/killtime.class.php");

//セッションスタート
session_start();

//データベース接続
DataBase::connect($conf);

//インスタンス作成
$killtumeupdata_instance = new KillTime($config);

//タイムアウト更新
$killtumeupdata_instance->KillTimeUpdate($_SESSION["login_token"]);

//ログインステータス
echo $killtumeupdata_instance->GetStatus();

//データベース切断
DataBase::DisConnect();

?>
