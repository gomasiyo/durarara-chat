<?php
/**
 *  Chat - Insert.System
 *  Author : Goma.Nanoha
 */

//Header javaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/insert.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/token.class.php");
require_once("../class/insert.class.php");

//データベース接続
DataBase::connect($conf);

//セッションスタート
session_start();

//フォーム代入
$u_token = $_SESSION["login_token"];
$s_token = $_SESSION["token"];
$p_token = $_POST["token"];
$log = $_POST["log"];

//インスタンス作成
$insert_instance = new Insert($config);

//投稿処理
$insert_instance->InsertLog( $u_token, $s_token, $p_token, $log);

//ステータス出力
echo $insert_instance->GetStatus();

$_SESSION["token"] = null;

//データベース切断
DataBase::DisConnect();


