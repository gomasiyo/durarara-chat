<?php
/**
 *  Chat - AddAccount.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル

require_once("../config/database.config.php");
require_once("../config/regist.config.php");
require_once("../class/database.class.php");
require_once("../class/API.class.php");
require_once("../class/regist.class.php");
require_once("../class/token.class.php");

session_start();

//データベース接続
DataBase::connect($conf);

    //インスタンス作成
    $Regist = new Regist($config);

    //アカウント作成
    $Regist->AddAccount( $_POST["login_id"], $_POST["screen_name"], $_POST["passwd"], $_POST["img_id"], $_POST["theme_id"], $_POST["token"], $_SESSION["token"]);

    //ステータス出力
    echo $Regist->GetStatus();

    $_SESSION["token"] = null;

//データベース切断
DataBase::disconnect();
