<?php
/**
 *  Chat - CheckLogin.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/checklogin.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/sessions.class.php");

//セッションスタート
session_start();

//データベース接続
DataBase::connect($conf);

//インスタンス作成作成
$checklogin_instance = new Sessions($config);

if($checklogin_instance->SetLoginId($_SESSION["login_id"])) {

    $checklogin_instance->CheckNowLogIn();

}

//ステータス出力
echo $checklogin_instance->GetStatus();

//データベース切断
DataBase::DisConnect();

?>
