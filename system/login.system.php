<?php
/**
 *  Chat - Login.System
 *  Author : Goma.Nanoha
 */

//Header JavaScript Setting
header('Content-Type: text/javascript; charset=utf-8');

//インクルードファイル
require_once("../config/database.config.php");
require_once("../config/login.config.php");
require_once("../config/systemlog.config.php");
require_once("../class/API.class.php");
require_once("../class/database.class.php");
require_once("../class/token.class.php");
require_once("../class/sessions.class.php");

//セッションスタート
session_start();

//データベース接続
DataBase::connect($conf);

//Users & Password
$login_id = $_POST["login_id"];
$passwd = $_POST["passwd"];

//インスタンス作成
$login_instance = new Sessions($config);

//IDチェック
if($login_instance->Login( $login_id, $passwd)) {

    //現在ログインしているか
    if(!$login_instance->CheckNowLogIn()) {

        //ログイン処理
        $login_instance->SetNowLogIn();
        $login_instance->SetLoginStatusMessage();

    }

    //ログイントークン代入
    $_SESSION["login_token"] = $login_instance->GetNowLogInToken();
    //ログインID代入
    $_SESSION["login_id"] = $login_id;

}

//ログインstatus
echo $login_instance->GetStatus();

//データベース切断
DataBase::DisConnect();

