<?php
/**
 *  Chat : DataBase.config
 *  Author Goma.Nanoha
*/

//データベース関係
    //データベース設定(Mysql)
    $db = "mysql";
    //データベースホスト
    $host = "localhost";
    //データベース名
    $dbname = "chat";

    //ユーザー
    $dbuser = "root";
    //パスワード
    $dbpasswd = "root";

    //DataBase Set Conf
    $dsn = "$db:host=$host; dbname=$dbname";
    $conf = array("dsn" => $dsn, "user" => $dbuser, "pass" => $dbpasswd);

//End DataBase

?>
