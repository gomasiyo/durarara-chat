<?php
/**
 *  Chat : SystemLog.Config
 *  Author Goma.Nanoha
*/

//SystemLog (%1 ユーザー名代入)
    //ログインログ
    $config["System_LogIn"] = "--%1さんがログインしました";
    //ログアウトログ
    $config["System_LogOut"] = "--%1さんがログアウトしました";
    //セッション切れログ
    $config["System_Kill"] = "--%1さんがタイムアウトしました";

?>
