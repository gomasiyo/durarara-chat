$(function() {

    // システムファイル
    var AddAccount = "./system/addaccount.system.php";
    var CheckLogin = "./system/checklogin.system.php";
    var Insert = "./system/insert.system.php";
    var KillTimeKill = "./system/killtimekill.system.php";
    var KillTimeUpdate = "./system/killtimeupdate.system.php";
    var LogAll = "./system/logall.system.php";
    var LogIn = "./system/login.system.php";
    var LogOut = "./system/logout.system.php";
    var LogUpdate = "./system/logupdate.system.php";
    var NowLogIn = "./system/nowlogin.system.php";
    var Token = "./system/token.system.php";

    // テンプレートファイル
    var AddAccountTmpl = "./tmpl/addaccount.tmpl.html";
    var LogInTmpl = "./tmpl/login.tmpl.html";
    var LogsTmpl = "./tmpl/logs.tmpl.html";
    var SystemLogsTmpl = "./tmpl/systemlogs.tmpl.html";
    var PostTempl = "./tmpl/post.tmpl.html";

    // システムコード
    var d = document;
    var w = window;

    // ページ遷移処理
    var LoadingPage = {

        setPages : function( PageName ) {

            switch( PageName ) {
                case "LogIn" :
                    // ログインページ遷移
                    this.dellPages();
                    this.getLogInForm();
                    $( "#header_login" ).slideDown( "normal" );
                    break;
                case "AddAccount" :
                    // 新規登録ページ遷移
                    this.dellPages();
                    this.getAddAccountForm();
                    // 可動式ヘッダーのためここでサイズ指定
                    $( "#header_add" ).css( "height" , 400 );
                    $( "#header_add" ).slideDown( "normal" );
                    break;
                case "Post" :
                    // ポストページ遷移
                    this.dellPages();
                    this.getPostForm();
                    $( "#header_post" ).slideDown( "normal" );
                    break;
            }

        },

        dellPages : function() {

            // ページ前のデータ消去
            $( "#header > article" ).slideUp( "normal" );
            $( "#header_login" ).empty();
            $( "#header_add" ).empty();
            $( "#header_post" ).empty();

        },

        getLogInForm : function() {

            $.ajax({
                type : 'GET',
                dataType : 'html',
                url : LogInTmpl,
                cache : false,
                success : function( Pages ) {
                    $( "#header_login" ).append( Pages );
                }
            });

        },

        getPostForm : function() {

            $.ajax({
                type : 'GET',
                dataType : 'html',
                url : PostTempl,
                cache : false,
                success : function( Pages ) {
                    $( "#header_post" ).append( Pages );
                    TokenSystem.resetToken( "post" );
                }
            });

        },

        getAddAccountForm : function() {

            $.ajax({
                type : 'GET',
                dataType : 'html',
                url : AddAccountTmpl,
                cache : false,
                success : function( Pages ) {
                    $( "#header_add" ).append( Pages );
                    TokenSystem.resetToken( "add" );
                }
            });

        }

    }

    // エラーメッセージ処理
    var Information = {

        setErrorMessage : function( Message ) {

            // エラーメッセージ表示
            switch( Message ) {
                case "LogIn" :
                    this.setLoginError();
                    $( "#header_error" ).slideDown( "fast" );
                    break;
                case "AddAccount" :
                    this.setAddAccountError();
                    $( "#header_error" ).slideDown( "fast" );
                    break;
                case "Post" :
                    this.setPostError();
                    $( "#header_error" ).slideDown( "fast" );
                    break;
                case "Other" :
                    this.setOtherError();
                    $( "#header_error" ).slideDown( "fast" );
                    break;
                default :
                    // その他のエラー文
                    $( "#error_message" ).append( Message );
                    $( "#header_error" ).slideDown( "fast" );
                    break;
            }

        },

        dellErrorMessage : function() {

            // エラーメッセージ消去
            $( "#add_form_login_id_errors , #add_form_screen_name_errors , #add_form_passwd_errors , #add_form_img_id_errors , #add_form_theme_id_errors" ).empty();
            $( "#add_form_login_id_errors , #add_form_screen_name_errors , #add_form_passwd_errors , #add_form_img_id_errors , #add_form_theme_id_errors" ).slideUp( "fast" );

            // Information消去
            $( "#error_message" ).empty();
            $( "#header_error" ).slideUp( "fast" );

            // エラー項目消去
            $( ".errors_message" ).removeClass( "errors_message" );

        },

        setLoginError : function() {

             // ログインエラーメッセージ
            $( "#error_message" ).append( "ログインできませんでした。赤い項目をご確認ください。" );

        },

        setAddAccountError : function() {

            // 新規登録エラーメッセージ
            $( "#error_message" ).append( "新規登録が出来ませんできた。赤い項目をご確認ください。" );

        },

        setPostError : function() {

            // ポストエラーメッセージ
            $( "#error_message" ).append( "ポストできませんでした。再度ロードしても治らない場合は管理者にご連絡ください。" );

        },

        setOtherError : function() {

            // エラーメッセージ表示
            $( "#error_message" ).append( "不明なエラーが起きました。再度ロードしても治らない場合は管理者に連絡ください。" );

        }

    }

    // ログイン処理
    var LogInSystem = {

        LogIn : function() {

            // 両方起動
            this.clickLogIn();
            this.enterLogIn();
            this.backLogIn();

        },

        setLogIn : function() {

            $.ajax({
                type : 'POST',
                dataType : 'json',
                url : LogIn,
                data : {
                    login_id : $( "#login_form input[name=login_id]" ).val(),
                    passwd : $( "#login_form input[name=passwd]" ).val()
                },
                success : function( StatusCode ) {

                    // エラーメッセージ消去
                    Information.dellErrorMessage();

                    // ログイン処理
                    if( StatusCode.Status == "OK" ) {
                        // ポスト画面遷移
                        LoadingPage.setPages( "Post" );
                    } else
                    if( StatusCode.Status == "Error" ) {

                        // エラーメッセージ
                        Information.setErrorMessage( "LogIn" );

                        if( StatusCode.ErrorCode == "NotUser" ) {
                            // IDまたはパスワードが間違っている
                            $(" #login_form dt" ).addClass( "errors_message" );
                        } else {

                            // エラー項目表示
                            $.each( StatusCode.ErrorCode.EmptyError , function( i , val ){
                                $( "#login_form_" + val ).addClass( "errors_message" );
                            });

                        }

                    } else {
                        // エラーメッセージ
                        Information.setErrorMessage( "Other" );
                    }

                }
            });

        },

        clickLogIn : function() {

            // クリック後処理
            $( "#login_form_submit_post" ).live( "click" , function() {
                LogInSystem.setLogIn();
            });

        },

        enterLogIn : function() {

            // Enter後の処理
            $( "#header_login" ).keypress( function( e ) {

                if( e.which && e.which == 13 ) {
                    LogInSystem.setLogIn();
                }

            });

        },

        backLogIn : function() {

            // クリック後処理
            $( "#add_form_submit_cancel" ).live( "click" , function() {
                LoadingPage.setPages( "LogIn" );
            });

        }

    }

    // ログアウト処理
    var LogOutSystem = {

        LogOut : function() {

            // 起動
            this.ShortcutLogOut();

        },

        getLogOut : function() {

            $.ajax({
                type : 'GET',
                dataType : 'json',
                url : LogOut,
                success : function( StatusCode ) {

                    // エラー・メッセージ消去
                    Information.dellErrorMessage();

                    // メッセージ追加
                    if( StatusCode.Status == "OK" ) {
                        // ログアウト成功
                        Information.setErrorMessage( "ログアウトしました" );
                        LoadingPage.setPages( "LogIn" );
                    } else
                    if( StatusCode.Status == "Error" ) {
                        // ログアウト失敗
                        Information.setErrorMessage( "現在ログインしていません。" );
                    } else {
                        // 不明なエラー
                        Information.setErrorMessage( "Other" );
                    }
                }
            });

        },

        ShortcutLogOut : function() {

            $( d ).on( "keydown" , d , function( e ) {
                // [ l ]が押された場合
                if( e.which === 76 ){
                    // alt+[ l ]が押された場合
                    if( e.altKey ) {
                        LogOutSystem.getLogOut();
                    } else {
                        $.noop();
                    }
                } else {
                    $.noop();
                }
            });

        }

    }
    LogOutSystem.LogOut();
    // 新規登録処理
    var AddAccountSystem = {

        AddAccount : function() {

            // 同時起動
            this.clickAddAccount();
            this.submitAddAccount();
            this.enterAddAccount();

        },

        setAddAccount : function() {

            $.ajax({
                type : 'POST',
                dataType : 'json',
                url : AddAccount,
                data : {
                    token : $( "#add_form input[name=token]" ).val(),
                    login_id : $( "#add_form input[name=login_id]" ).val(),
                    screen_name : $( "#add_form input[name=screen_name]" ).val(),
                    passwd : $( "#add_form input[name=passwd]" ).val(),
                    img_id : $( "#add_form input[name=img_id]:checked" ).val(),
                    theme_id : $( "#add_form input[name=theme_id]:checked" ).val()
                },
                success : function( StatusCode ) {

                    // エラー・メッセージ消去
                    Information.dellErrorMessage();

                    // 新規登録処理
                    if( StatusCode.Status == "OK" ) {
                        // 登録成功
                        LoadingPage.setPages( "LogIn" );
                        Information.setErrorMessage( "アカウントが作成されました。ログインフォームでログインしてください。" );
                    } else
                    if( StatusCode.Status == "Error" ) {

                        // エラー表示のためサイズ設定リセット
                        $( "#header_add" ).css( "height" , '' );

                        if( StatusCode.ErrorCode.TokenError == "g_token" ) {
                            Information.setErrorMessage( "Other" );
                        } else {

                            // エラー項目表示
                            $.each( StatusCode.ErrorCode , function( key , val ) {

                                // エラー挿入
                                switch( key ) {
                                    case "EmptyError" :
                                        $.each( val , function( key , val ) {
                                            $( "#add_form_" + val ).addClass( "errors_message" );
                                            $( "#add_form_" + val + "_errors" ).append( "<p>項目に入力してください</p>" );
                                            $( "#add_form_" + val + "_errors" ).slideDown( "fast" );
                                        });
                                        break;
                                    case "EmError" :
                                        $.each( val , function( key , val ) {
                                            $( "#add_form_" + val ).addClass( "errors_message" );
                                            $( "#add_form_" + val + "_errors" ).append( "<p>項目の中に半角英数字以外は使えません。</p>" );
                                            $( "#add_form_" + val + "_errors" ).slideDown( "fast" );
                                        });
                                        break;
                                    case "SameError" :
                                        $.each( val , function( key , val ) {
                                            $( "#add_form_" + val ).addClass( "errors_message" );
                                            $( "#add_form_" + val + "_errors" ).append( "<p>現在使われています。</p>" );
                                            $( "#add_form_" + val + "_errors" ).slideDown( "fast" );
                                        });
                                        break;
                                }

                            });

                        }

                        // 処理終了後トークンの再設定
                        TokenSystem.resetToken( "add" );

                    }

                }
            });

        },

        clickAddAccount : function() {

            // 新規登録画面遷移
            $( "#login_form_submit_addaccount" ).live( "click" , function() {
                LoadingPage.setPages( "AddAccount" );
            });

        },

        submitAddAccount : function() {

            // クリック後の処理
            $( "#add_form_submit_post" ).live( "click" , function() {
                AddAccountSystem.setAddAccount();
            });

        },

        enterAddAccount : function() {

            // Enter後の処理
            $( "#header_add" ).keypress( function( e ) {

                if( e.which && e.which == 13 ) {
                    AddAccountSystem.setAddAccount();
                }

            });

        }

    }

    // 投稿処理
    var PostSystem = {

        // 最後のログを保存
        log : {},

        Post : function() {

            // 同時起動
            this.clickPost();
            this.enterPost();

        },

        setPost : function() {

            if( this.log != $( "#post_form textarea" ).val() ) {

                // ログ保存
                this.log = $( "#post_form textarea" ).val();

                $.ajax({
                    type : 'POST',
                    dataType : 'json',
                    url : Insert,
                    data : {
                        token : $( "#post_form input[name=token]" ).val(),
                        log : $( "#post_form textarea" ).val()
                    },
                    success : function( StatusCode ) {

                        // エラー・メッセージ消去
                        Information.dellErrorMessage();

                        // 例外エラー表示
                        if( StatusCode.Status == "OK" ) {
                            // 投稿できた場合はFormの中消去
                            $( "#post_form textarea" ).val( "" );
                        } else
                        if ( StatusCode.Status == "Error" ) {

                            if( StatusCode.ErrorCode.EmptyError == "log" ) {
                                // 投稿エラーでかつログだけがない場合。
                                $( "#post_form textarea" ).val( "" );
                            } else
                            if( StatusCode.ErrorCode.GetUserIdError == "u_token" ) {
                                Information.setErrorMessage( "ユーザーが確認できません。ログアウトしてログインしなおしてください。" );
                            } else
                            if( StatusCode.ErrorCode.TokenError == "get_token" ) {
                                Information.setErrorMessage( "トークンセキュリティーが起動しました。リロードしても治らない場合は管理者に連絡してください。" );
                            } else {
                                // 不明なエラー
                                Information.setErrorMessage( "Post" );
                            }

                        } else {
                            // 不明なエラー
                            Information.setErrorMessage( "Post" );
                        }

                        TokenSystem.resetToken( "post" );

                    }
                });

            } else {

                // 同じ投稿の場合Formの中消去
                $( "#post_form textarea" ).val( "" );

            }

        },

        clickPost : function() {

            // クリック後の処理
            $( "#post_form_submit" ).live( "click" , function() {
                PostSystem.setPost();
            });

        },

        enterPost : function() {

            $( d ).on( "keypress" , "#post_form textarea" , function( e ) {
                // Enterが押された場合
                if( e.which === 13 ){
                    // Shift+Enterが押された場合
                    if( e.shiftKey ) {
                        $.noop();
                    } else {
                        PostSystem.setPost();
                        $( "#post_form textarea" ).val( "" );
                    }
                } else {
                    $.noop();
                }
            });

        }

    }

    // トークン処理
    var TokenSystem = {

        setToken : function( FormID , Token ) {

            // 取得できない場合はエラー
            if( Token == "" ) {
                LoadingPage.setPages( "Other" );
            } else {
                // 代入処理
                $( "#" + FormID + "_form input[name=token]" ).val( Token );
            }

        },

        resetToken : function( FormID ) {

            // 消して再度代入
            this.dellToken( FormID );
            this.getToken( FormID );

        },

        getToken : function( FormID ) {

            $.ajax({
                type :'GET',
                dataType : 'json',
                url : Token,
                success : function( Token ) {
                    TokenSystem.setToken( FormID , Token.Token );
                }
            });

        },

        dellToken : function( FormID ) {

            // トークン消去
            $( "#" + FormID + "_form input[name=token]" ).val( "" );

        }

    }

    // ログ処理
    var LogSystem = {

        // Template保存
        LogTemplate : {},
        SystemTemplate : {},

        Log : function() {

            // 全て起動
            this.getLogTemplate();
            this.getSystemTemplate();
            this.getAllLog();
            
            // ログ更新
            setInterval( function() {
                LogSystem.getUpdateLog();
            } , 500 );

        },

        getLogTemplate : function() {

            // 妥協して同期通信でテンプレートダウンロード
            $.ajax({
                type : 'GET',
                dataType : 'html',
                url : LogsTmpl,
                async: false,
                success : function( Template ) {
                    LogSystem.LogTemplate = Template;
                }
            });

        },

        getSystemTemplate : function() {

            // 妥協して同期通信でテンプレートダウンロード
            $.ajax({
                type : 'GET',
                dataType : 'html',
                url : SystemLogsTmpl,
                async: false,
                success : function( Template ) {
                    LogSystem.SystemTemplate = Template;
                }
            });

        },

        getAllLog : function() {

            // ログをすべてダウンロード
            $.ajax({
                type : 'GET',
                dataType : 'json',
                url : LogAll,
                success : function( LogJson ) {

                    $.each( LogJson.Log , function( key , val ) {

                        // システムログの場合は、システムログ用のModeling使用
                        if( val.login_id == "SystemLog" ) {
                            LogSystem.setModelingSystem( val );
                        } else {
                            // 通常ログModeling
                            LogSystem.setModelingLog( val );
                        }

                    });

                }
            });

        },

        getUpdateLog : function() {

            // ログの最終ID取得
            var MaxId = $( ".logs:first-child" ).data( "logid" );
            
            // アップデート用ログをダウンロード
            $.ajax({
                type : 'POST',
                dataType : 'json',
                url : LogUpdate,
                data : { max_id : MaxId },
                success : function( LogJson ) {

                    // 更新があれば出力
                    if( LogJson.Status == "OK" ) {

                        $.each( LogJson.Log , function( key , val ) {

                            // システムログの場合は、システムログ用のModeling使用
                            if( val.login_id == "SystemLog" ) {
                                LogSystem.setModelingSystem( val );
                            } else {
                                // 通常ログModeling
                                LogSystem.setModelingLog( val );
                            }

                        });

                    }

                }
            });

        },

        setModelingLog : function( LogJson ) {

            // 通常ログ整形
            $.tmpl( this.LogTemplate , this.extendModeling( LogJson ) ).prependTo( "#log" );

        },

        setModelingSystem : function( LogJson ) {

            // システムログ整形
            $.tmpl( this.SystemTemplate , LogJson ).prependTo( "#log" );

        },

        extendModeling : function( LogObjct ) {

            // エスケープ関数
            function escapeHTML( HTML ) {
                return HTML.replace( /[&"<>]/g , function(c) {
                    return {
                        "&" : "&amp;",
                        '"' : "&quot;",
                        "<" : "&lt;",
                        ">" : "&gt;"
                    }[c];
                });
            }

            // 改行コードを改行タグに変更
            var Log = LogObjct.log;
            Log = escapeHTML( Log );
            var extend = { log : Log.replace( /\n/g,  "<br>") };
            LogObjct = $.extend( LogObjct , extend );

            // 変更後出力
            return LogObjct;

        }

    }

    // キルタイム処理
    KillTimeSystem = {

        KillTime : function() {

            // 同時起動
            setInterval( function() {
                KillTimeSystem.KillTimeKill();
                KillTimeSystem.KillTimeUpdate();
            } , 900 );

        },

        KillTimeKill : function() {

            // アクセスし、キルタイム処理
            $.ajax({
                type : 'GET',
                dataType : 'json',
                url : KillTimeKill
            });

        },

        KillTimeUpdate : function() {

            // アクセスし、キルタイムアップデート処理
            $.ajax({
                type : 'GET',
                dataType : 'json',
                url : KillTimeUpdate
            });

        }

    }

    // Start - ページロード処理

    $.ajax({
        type : 'GET',
        dataType : 'json',
        url : CheckLogin,
        success : function( StatusCode ) {

            // エラーメッセージ消去
            Information.dellErrorMessage();

            if( StatusCode.Status == "OK" ) {
                // ポストページ遷移
                LoadingPage.setPages( "Post" );
            } else
            if( StatusCode.Status == "Error" ) {
                // ログインページ遷移
                LoadingPage.setPages( "LogIn" );
            } else {
                // エラーメッセージ表示
                Information.setErrorMessage( "Other" );
            }

        }
    });

    // End - ページロード処理

    // Start - 処理実行郡

        // ログ処理
        LogSystem.Log();

        // ログイン処理
        LogInSystem.LogIn();

        // 新規登録処理
        AddAccountSystem.AddAccount();

        // ポスト処理
        PostSystem.Post();

        // キルタイム処理
        KillTimeSystem.KillTime();

    // End - 処理実行部

});
