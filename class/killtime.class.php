<?php
/**
 *  Chat - KillTime.Class
 *  Author : Goma.Nanoha
 */

class KillTime extends API {

    private $KillUserLog;
    private $User;

    public function KillTimeKill() {

        $sql = 'SELECT login_id FROM login WHERE kill_time < ?';
        $stmt = $this->Db->prepare($sql);
        $stmt->execute( array(
            time()
        ));
        $killuser = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($killuser)) {

            $this->Status = array(
                "Status" => "OK",
                "StatusCode" => "KillTimeKill"
            );

        $this->KillUser($killuser);

        } else {

            $this->Status = array(
                "Status" => "Error",
                "ErrorCode" => "NoKillUser"
            );

        }

    }

    //該当ユーザータイムアウト
    private function KillUser($killuser) {

        //タイムアウト用SQL
        $sql = 'DELETE FROM login WHERE login_id = ?';
        $stmt = $this->Db->prepare($sql);

        foreach( $killuser as $val) {

            $this->User = $val;
            $flag = $stmt->execute( array(
                $this->User['login_id']
            ));

            if($flag) {

                //システムログ
                $this->SystemLogInsert( "KillUser", $this->User['login_id']);

                $this->Status['KillUser'][] = $this->User['login_id'];

            } else {
                $this->Status['KillUser'][] = "Error".$this->User['login_id'];
            }

        }


    }

    public function KillTimeUpdate($token) {

        //空チェック
        $this->CheckEmpty( "sessions_token", $token);

        //既存チェック
        $this->CheckErrorStatus();

        if(empty($this->Status)) {

            //タイムアウトUpdate
            $sql = 'UPDATE login SET kill_time = ? WHERE sessions_token = ?';
            $stmt = $this->Db->prepare($sql);
            $flag = $stmt->execute( array(
                time() + $this->Config["UpdateTime"],
                $token
            ));

            if($flag) {

                $this->Status = array(
                    "Status" => "OK",
                    "StatusCode" => "KillTimeUpdate"
                );

            } else {

                $this->Status = array(
                    "Status" => "Error",
                    "ErrorCode" => "KillTimeUpdate"
                );

            }

        }

    }

}

?>
