<?php
/**
 * Chat - Logs.Class
 * Author : Goma.Nanoha
 */

class Logs extends API {

    public function LogAll() {

        $sql = 'SELECT log.id, log.login_id, user.screen_name, log.log, user.img_id, user.theme_id FROM log INNER JOIN user ON log.login_id = user.login_id ORDER BY  log.id ASC';
        $stmt = $this->Db->query($sql);
        $logs = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $this->Status = array(
            "Status" => "OK",
            "Log" => $logs
        );

    }

    public function LogUpdate($max_id) {

        //空チェック
        $this->CheckEmpty( "max_id", $max_id);
        //数値チェック
        $this->CheckNumber( "max_id", $max_id);

        //Error代入
        $this->CheckErrorStatus();

        if(empty($this->Status)) {

            $sql = 'SELECT log.id, log.login_id, user.screen_name, log.log, user.img_id, user.theme_id FROM log INNER JOIN user ON log.login_id = user.login_id WHERE log.id > ? ORDER BY  log.id ASC';
            $stmt = $this->Db->prepare($sql);
            $stmt->execute(array($max_id));
            $logupdate = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if(empty($logupdate)) {

                $this->Status  = array(
                    "Status" => "Error",
                    "ErrorCode" => "LogUpdate"
                );

            } else {

                $this->Status = array(
                    "Status" => "OK",
                    "Log" => $logupdate
                );

            }

        }

    }

}


?>
