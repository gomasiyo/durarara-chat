<?php
/**
 *  Chat - Insert.Class
 *  Author : Goma.Nanoha
 */

class Insert extends API {

    private $UserID;

    public function InsertLog( $u_token, $s_token, $p_token, $log) {

        //EmptyCheck
        $this->CheckEmpty( "u_token", $u_token);
        $this->CheckEmpty( "s_token", $s_token);
        $this->CheckEmpty( "p_token", $p_token);
        $this->CheckEmpty( "log", $log);
        //TokenCheck
        $this->CheckToken( "get_token", $s_token, $p_token);
        //TokenからID
        $this->CheckGetUserId( "u_token", $u_token);

        //エラーステータスチェック
        $this->CheckErrorStatus();

        if(empty($this->Status)) {

            $sql = 'INSERT INTO log (login_id, log) VALUES ( ?, ?)';
            $stmt = $this->Db->prepare($sql);
            $flag = $stmt->execute(array(
                $this->GetUserId,
                $log
            ));

            if($flag) {

                $this->Status = array(
                    "Status" => "OK",
                    "StatusCode" => "Insert"
                );

            } else {

                $this->Status = array(
                    "Status" => "Error",
                    "ErrorCode" => "Insert"
                );

            }

        }

    }

}

?>
