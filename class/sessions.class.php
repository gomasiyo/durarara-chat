<?php
/**
 *  Chat - Sessions.Class
 *  Author : Goma.Nanoha
 */

class Sessions extends API {

    private $LogInId;
    private $Passwd;
    private $LogInToken;

    public function LogIn( $login_id, $passwd) {

        //ID&パスワード保存
        $this->LogInId = $login_id;
        $this->Passwd = $passwd;

        //空チェック
        $this->CheckEmpty( "login_id", $login_id);
        $this->CheckEmpty( "passwd", $passwd);

        //既存チェック
        $this->CheckErrorStatus();

        if(empty($this->Status)) {

            $sql = 'SELECT * FROM user WHERE login_id = ? AND passwd = ?';
            $stmt = $this->Db->prepare($sql);
            $stmt->execute( array(
                $this->LogInId,
                sha1($this->Passwd)
            ));
            $login_status = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if(!empty($login_status)) {

                $this->Status = array(
                    "Status" => "OK",
                    "StatusCode" => "LogIn"
                );

                return true;

            } else {

                $this->Status = array(
                    "Status" => "Error",
                    "ErrorCode" => "NotUser"
                );

                return false;

            }

        } else {
            return false;
        }

    }

    public function LogOut( $login_id, $token) {

        //ID＆トークン保存
        $this->LogInId = $login_id;
        $this->LogInToken = $token;

        //空チェック
        $this->CheckEmpty( "login_id", $login_id);
        $this->CheckEmpty( "Token", $token);

        //既存チェック
        $this->CheckErrorStatus();

        if(empty($this->Status)) {

            $sql = 'SELECT * FROM login WHERE login_id = ? AND sessions_token = ?';
            $stmt = $this->Db->prepare($sql);
            $stmt->execute( array(
                $this->LogInId,
                $this->LogInToken
            ));

            $logout_status = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if(!empty($logout_status)) {

                $this->Status = array(
                    "Status" => "OK",
                    "StatusCode" => "LogOut"
                );

                return true;

            } else {

                $this->Status = array(
                    "Status" => "Error",
                    "ErrorCode" => "LogOut"
                );

                return false;

            }

        } else {
            return false;
        }

    }

    public function CheckNowLogIn() {

        $sql = 'SELECT login_id FROM login WHERE login_id = ?';
        $stmt = $this->Db->prepare($sql);
        $stmt->execute( array(
            $this->LogInId
        ));
        $checklogin = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($checklogin)) {

            $this->Status = array(
                "Status" => "OK",
                "StatusCode" => "NowLogin"
            );

            return true;

        } else {

            $this->Status = array(
                "Status" => "Error",
                "ErrorCode" => "NoLogin"
            );

            return false;

        }

    }

    public function SetNowLogin() {

        $sql = 'INSERT INTO login ( login_id, sessions_token, kill_time) VALUES ( ?, ?, ?)';
        $stmt = $this->Db->prepare($sql);
        $flag = $stmt->execute( array(
            $this->LogInId,
            $this->Token,
            time() + $this->Config["KillTime"]
        ));

        if($flag) {
            $this->SystemLogInsert( "LogIn", $this->LogInId);
        } else {

            $this->Status = array(
                "Status" => "Error",
                "ErrorCode" => "SetNowLogIn"
            );

        }

    }

    public function DelNowLogIn() {

        $sql = 'DELETE FROM login WHERE sessions_token = :token';
        $stmt = $this->Db->prepare($sql);
        $stmt->bindValue(':token', $this->LogInToken);
        $flag = $stmt->execute();

        if($flag) {
            $this->SystemLogInsert( "LogOut", $this->LogInId);
        } else {

            $this->Status = array(
                "Status" => "Error",
                "ErrorCode" => "DelNowLogIn"
            );

        }

    }

    public function NowLogIn() {

        $sql = 'SELECT login_id FROM login WHERE 1';
        $stmt = $this->Db->query($sql);
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($user)) {

            $this->Status = array(
                "Status" => "OK",
                "LogInUser" => $user
            );

        } else {

            $this->Status = array(
                "Status" => "Error",
                "ErrorCode" => "NoLogIn"
            );

        }

    }

    public function GetNowLogInToken() {

        $sql = 'SELECT sessions_token FROM login WHERE login_id = ?';
        $stmt = $this->Db->prepare($sql);
        $stmt->execute( array(
            $this->LogInId
        ));
        $gettoken = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $gettoken[0]["sessions_token"];

    }

    public function SetLoginId($id) {

        if(!empty($id)) {

            //入っていたら代入
            $this->LogInId = $id;

            return true;

        } else {

            $this->Status = array(
                "Status" => "Error",
                "ErrorCode" => "NoLogin"
            );

            return false;

        }

    }

    public function SetLoginStatusMessage() {

        $this->Status = array(
            "Status" => "OK",
            "StatusCode" => "LogIn"
        );

    }

}

?>
