<?php
/**
 *  ShortBBS Chat - DataBase.Class
 *  Author : Goma.Nanoha
*/

//DataBase接続Class
final class DataBase {

    private static $instance;

    private function __construct() {

    }

    //SQL接続
    public static function Connect($conf) {

        if(!isset(self::$instance)) {
            try {
                self::$instance = new PDO( $conf["dsn"], $conf["user"], $conf["pass"], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"));
            } catch (PDOException $dev) {
                exit('ErrerAlert: データベースに接続できませんでした。' . $dev->getMessage());
            }
        }

        return self::$instance;

    }

    //二回目以降SQL接続
    public static function Singleton() {

        if(!isset(self::$instance)) {
            die('DataBase Not Connect');
        }

        return self::$instance;

    }

    //SQL切断
    public static function DisConnect() {
        self::$instance = null;
    }

    public final function __clone() {
        throw new RuntimeException('Clone is Allowed against' .get_class($this));
    }

}
?>
