<?php
/**
 *  Chat : Regist.Class
 *  Author Goma.Nanoha
 */

//アカウント関係class
class Regist extends API {

    //アカウント追加メソッド
    public function AddAccount( $login_id, $screen_name, $passwd, $img_id, $theme_id, $p_token, $s_token) {

        //EmptyCheck
        $this->CheckEmpty( "login_id", $login_id);
        $this->CheckEmpty( "screen_name", $screen_name);
        $this->CheckEmpty( "passwd", $passwd);
        $this->CheckEmpty( "img_id", $img_id);
        $this->CheckEmpty( "theme_id", $theme_id);
        $this->CheckEmpty( "p_token", $p_token);
        $this->CheckEmpty( "s_token", $s_token);
        //TokenCheck
        $this->CheckToken( "g_token", $s_token, $p_token);
        //SameCheck
        $this->CheckSame( "login_id", $login_id);
        $this->CheckSame( "screen_name", $screen_name);
        //IDCheck
        $this->CheckEm( "login_id", $login_id);
        $this->CheckEm( "passwd", $passwd);

        //エラーステータスチェック
        $this->CheckErrorStatus();

        if(empty($this->Status)) {

            $sql = "INSERT INTO user ( login_id, screen_name, passwd, img_id, theme_id) VALUES (?, ?, ?, ?, ?)";
            $stmt = $this->Db->prepare($sql);
            $flag = $stmt->execute(array(
                $login_id,
                $screen_name,
                sha1($passwd),
                $img_id,
                $theme_id
            ));

            if($flag) {

                $this->Status = array(
                    "Status" => "OK",
                    "StatusCode" => "AddAccount"
                );

            } else {

                $this->Status = array(
                    "Status" => "Error",
                    "ErrorCode" => "AddAccount"
                );

            }

        }

    }

    //後ほど作成
    public function DelAccount() {
    }

}


?>
