<?php
/**
 *  Chat - Token.Class
 *  Author : Goma.Nanoha
 */

//ワンタイムトークン作成クラス
final class Token {

    private $Token;
    private $EncodeToken;


    public function __construct() {
        $this->Token = md5(uniqid(mt_rand(), TRUE));
    }

    //トークン作成
    public function GetToken() {
        return $this->Token;
 }

    //トークン整形
    public function FairingToken($token) {

        //トークンが入っていれば整形
        if(!empty($token)) {

            //整形
            $this->EncodeToken = array(
                "Status" => "OK",
                "StatusCode" => "GetToken",
                "Token" => $token
            );

        } else {

            //Error整形
            $this->EncodeToken = array(
                "Status" => "Error",
                "ErrorCode" => "GetToken"
            );

        }

        return json_encode($this->EncodeToken);

    }

 }

