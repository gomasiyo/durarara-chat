<?php
/**
 *  Chat : API.Class
 *  Author : Goma.Nanoha
 */

 class API {

    protected $Db;
    protected $Status;
    protected $Token;
    protected $Config;
    protected $TokenCheck;
    protected $SameCheck;
    protected $EmptyCheck;
    protected $EmCheck;
    protected $NumberCheck;
    protected $GetUserIdCheck;
    protected $GetUserId;

    public function __construct($config) {

        //APIConfig
        if($config["API_DataBase"]){
            $this->SetDataBase();
        }
        if($config["API_Token"]){
            $this->SetToken();
        }

        //その他Config代入
        $this->Config = $config;

    }

    //データベースセット
    private function SetDataBase() {
        $this->Db = DataBase::Singleton();
    }

    //トークンセット
    private function SetToken() {

        $token = new Token();
        $this->Token = $token->GetToken();

    }

    //ステータス出力
    public function GetStatus() {
        return json_encode($this->Status);
    }

    //トークンチェック
    protected function CheckToken( $status_name, $s_token, $p_token) {

        if($s_token != $p_token) {
            $this->TokenCheck[] = $status_name;
        }

    }

    //半角チェック
    protected function CheckEm( $status_name, $check) {

        if(!preg_match( '/^[\w]+$/', $check)) {
            $this->EmCheck[] = $status_name;
        }

    }

    //空チェック
    protected function CheckEmpty( $status_name, $check) {

        if(empty($check) || mb_ereg_match( '^(\s|　)+$', $check)) {
            $this->EmptyCheck[] = $status_name;
        }

    }

    //被りCheck
    protected function CheckSame( $status_name, $check) {

        //スペース消去
        $check = preg_replace('/\s|　/','',$check);

        //DB被りCheck
        $sql = "SELECT * FROM user WHERE $status_name = ?";
        $checkdb = $this->Db->prepare($sql);
        $checkdb->execute(array(
            $check
        ));
        $log_all = $checkdb->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($log_all)) {
            $this->SameCheck[] = $status_name;
        }

    }

    protected function CheckNumber( $status_name, $check) {

        //数字化判断
        if(!preg_match('/^[0-9]+$/',$check)){
            $this->NumberCheck[] = $status_name;
        }

    }

    protected function CheckGetUserId( $status_name, $check) {

        $sql = 'SELECT login_id FROM login WHERE sessions_token = ?';
        $stmt = $this->Db->prepare($sql);
        $stmt->execute(array(
            $check
        ));
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($user)) {
            $this->GetUserId = $user[0]["login_id"];
        } else {
            $this->GetUserIdCheck[] = $status_name;
        }

    }

    protected function CheckErrorStatus() {

        if(!empty($this->GetUserIdCheck)|| !empty($this->TokenCheck) || !empty($this->EmptyCheck) || !empty($this->SameCheck) || !empty($this->EmCheck) || !empty($this->NumberCheck)) {

            $this->Status["Status"] = "Error";

            if(!empty($this->GetUserIdCheck)) {
                $this->Status["ErrorCode"]["GetUserIdError"] = $this->GetUserIdCheck;
            }

            if(!empty($this->TokenCheck)) {
                $this->Status["ErrorCode"]["TokenError"] = $this->TokenCheck;
            }

            if(!empty($this->EmptyCheck)) {
                $this->Status["ErrorCode"]["EmptyError"] = $this->EmptyCheck;
            }

            if(!empty($this->SameCheck)) {
                $this->Status["ErrorCode"]["SameError"] = $this->SameCheck;
            }

            if(!empty($this->EmCheck)) {
                $this->Status["ErrorCode"]["EmError"] = $this->EmCheck;
            }

            if(!empty($this->NumberCheck)) {
                $this->Status["ErrorCode"]["NumberError"] = $this->NumberCheck;
            }

        }

    }


    protected function SystemLogInsert( $status, $login_id) {

        $sql = 'SELECT screen_name FROM user WHERE login_id = ?';
        $stmt = $this->Db->prepare($sql);
        $stmt->execute( array(
            $login_id
        ));

        $set = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //スクリーンネーム代入
        $screen_name = $set[0]["screen_name"];

        $sql = "INSERT INTO log ( login_id, log) VALUES ( ?, ?)";
        $stmt = $this->Db->prepare($sql);

        switch($status) {

            //ログインログ
            case "LogIn":

                //名前変換
                $systemlog = preg_replace( '/\%1/', $screen_name, $this->Config["System_LogIn"]);
                break;

            case "LogOut":

                //名前変換
                $systemlog = preg_replace( '/\%1/', $screen_name, $this->Config["System_LogOut"]);
                break;

            case "KillUser":

                //名前変更
                $systemlog = preg_replace( '/\%1/', $screen_name, $this->Config["System_Kill"]);

        }

        $stmt->execute( array(
            "SystemLog",
            $systemlog
        ));

    }

 }
