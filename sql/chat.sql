-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成日時: 2013 年 4 月 24 日 01:52
-- サーバのバージョン: 5.5.25
-- PHP のバージョン: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- データベース: `chat`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `log`
--

CREATE TABLE `log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login_id` text,
  `log` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='ログテーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `login`
--

CREATE TABLE `login` (
  `login_id` text,
  `sessions_token` text,
  `kill_time` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ログイン者テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login_id` text,
  `screen_name` text,
  `passwd` text,
  `img_id` text,
  `theme_id` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='ユーザーテーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
